# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)



## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

### Screenshot

![](./screenshot.jpg)


### Links

- Solution URL: [https://gitlab.com/piort789/frontend-mentor-03-product-preview-card-component-main](https://gitlab.com/piort789/frontend-mentor-03-product-preview-card-component-main)
- Live Site URL: [https://piort789.gitlab.io/frontend-mentor-03-product-preview-card-component-main/](https://piort789.gitlab.io/frontend-mentor-03-product-preview-card-component-main/)


### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow


### What I learned

- how to import fonts using @import
- how to swap images between mobile and desktop version using <picture> tag
- how to use rem and em units in css


### Useful resources
- [CSS em and rem explained](https://www.youtube.com/watch?v=_-aDOAMmDHI) -helped me to learn about em and rem css units
- [Are you using the right CSS units?](https://www.youtube.com/watch?v=N5wpD9Ov_To) - Helped me to learn when to use witch css unit.



## Author

- GitLab - [Piotr Juszczak](https://gitlab.com/piort789)
- Frontend Mentor - [@Pio678](https://www.frontendmentor.io/profile/Pio678)

